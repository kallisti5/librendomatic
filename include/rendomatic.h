/*
 * Copyright 2013-2014, Alexander von Gluck IV <kallisti5@unixzen.com>
 * Distributed under the terms of the MIT License.
 */
#ifndef RENDOMATIC_H
#define RENDOMATIC_H


#include <inttypes.h>
#include <stddef.h>


#ifndef addr_t
typedef uintptr_t addr_t;
#endif


struct rendo_bo {
	volatile int lock;
		// Lock for object
	uint32_t	size;
		// Size of buffer object in bytes
	uint32_t	alignment;
		// Alignment requirement
	uint32_t	offset;
		// Offset from the beginning of the aperture
	uint32_t	flags;
		// Flags for BO
};


struct rendo_context {
	int		valid;
		// Is the context valid?
	int		handle;
		// File handle for ioctl's
	void*	backend;
		// Private pointer for backend use
};

/*! \brief Lock flags
 */
typedef enum {
	R_LOCK_READY		= 0x01, /*!< Wait until hardware is ready for DMA */
	R_LOCK_QUIESCENT	= 0x02, /*!< Wait until hardware quiescent */
	R_LOCK_FLUSH		= 0x04, /*!< Flush this context's DMA queue first */
	R_LOCK_FLUSH_ALL	= 0x08, /*!< Flush DMA queues first */
	R_HALT_ALL_QUEUES	= 0x10, /*!< Halt all current and future queues */
	R_HALT_CUR_QUEUES	= 0x20, /*!< Halt all current queues */
} rendo_lock_flags;

/*! \brief Initialize rendomatic context
 *
 *	The initial entry point for setting up a rendomatic
 *  context.
 */
int rendo_initialize(struct rendo_context* context);

/*! \brief Destroy rendomatic context
 *
 *	Closes open handles and destroys rendomatic context.
 */
void rendo_destroy(struct rendo_context* context);

/*! \brief Get card base address
 *
 *	Obtain the base address of the card
 */
addr_t rendo_base(struct rendo_context* context);

/*! \brief Lock graphics card
 *
 *  Performs an agressive lock of the card
 */
void rendo_lock(struct rendo_context* context, rendo_lock_flags flags);

/*! \brief Unlock graphics card
 *
 *  Unlocks the graphics card if locked
 */
void rendo_unlock(struct rendo_context* context);

/*! \brief Allocate buffer object
 *
 *	Allocates a buffer object
 */
struct rendo_bo* rendo_bo_map(struct rendo_context* context, size_t size,
	uint32_t alignment, uint32_t flags);

/*! \brief Deallocate (free) buffer object
 *
 *	Deallocates previously mapped buffer object
 */
void rendo_bo_unmap(struct rendo_context* context, struct rendo_bo* object);

/*! \brief Lock a buffer object
 *
 *  Locks a buffer object for reading / writing
 */
void rendo_bo_lock(struct rendo_bo* bo);

/*! \brief Lock a buffer object
 *
 *  Unlocks a buffer object
 */
void rendo_bo_unlock(struct rendo_bo* bo);


#endif /* RENDOMATIC_H */
