# librendomatic

A simple OpenGL rendering pipeline designed to be an easy DRI / DRM replacement.

## Documentation

See docs for complete documentation. ```mkdocs serve``` can be used to get the full documentation navigation.
