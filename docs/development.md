# Compiling

librendomatic should be compiled on the native platform once the dependencies have been met. 

* Build: ```scons```
* Clean: ```scons -c```

# Testing

After compiling, contributors should execute the ``run_tests`` script to begin running API
simulations against their local graphics rendering hardware.

## Coverage

Given the wide varaitions in consumer and professional graphics rendering hardware, It's
important to have complete coverage of all public API's.  Each public subsystem should be
thoroughly exercised to improve stability.
