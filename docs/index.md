# Introduction
librendomatic is a clean and flexible pipeline for hardware GPU memory and command management 

![Logo](images/librendomatic.png)

## Platform Support
The librendomatic library is designed to be 100% platform neutral giving API clients like
Mesa3D a generic pipeline to 3D Hardware under a wide set of system designs.

Initially, librendomatic is targetting the following platforms:

* Haiku (Native)
* Linux (DRI / DRM)

## Dependencies
librendomatic requires a few libraries to successfully compile and run.

### Linux (DEB)
These dependencies can be installed with apt

** Build Dependencies **

* libx11-dev
* libdri-dev
* libva-dev
* scons

** Runtime Dependencies **

* libdri
* libx11
* libva

### Linux (RPM)
These dependencies can be installed with yum or dnf

** Build Dependencies **

* libX11-devel
* libdri-devel
* libva-devel
* scons

** Runtime Dependencies **

* libdri
* libX11
* libva

