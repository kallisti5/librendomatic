/*
 * Copyright 2013-2017, Alexander von Gluck IV <kallisti5@unixzen.com>
 * Distributed under the terms of the MIT License.
 */


#include <stdio.h>
#include <time.h>
#include <inttypes.h>

#include "debug.h"
#include "rendomatic.h"


// Unit test parameters
#define LOCK_THREAD_NUM 2

int
main()
{
	time_t beg = time(NULL);
	struct rendo_context context;
	if (rendo_initialize(&context) != 0) {
		ERROR("context initalization failed.\n");
		return 1;
	}
	INFO("context initialization successful.\n");

	/// Test begin
	INFO("Locking GPU until DMA ready...\n");
	rendo_lock(&context, R_LOCK_READY);
	INFO("Lock obtained.\n");

	INFO("Unlocking GPU...\n");
	rendo_unlock(&context);
	/// Test end

	rendo_destroy(&context);
	INFO("context destruction successful.\n");

	time_t end = time(NULL);
	printf("Unit test completed in %f seconds\n", difftime(end, beg));

	return 0;
}
