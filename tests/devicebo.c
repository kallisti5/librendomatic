/*
 * Copyright 2013-2014, Alexander von Gluck IV <kallisti5@unixzen.com>
 * Distributed under the terms of the MIT License.
 */


#include <stdio.h>
#include <time.h>
#include <inttypes.h>

#include "debug.h"
#include "rendomatic.h"


// Unit test parameters
#define BO_TEST_NUM	512

int
main()
{
	time_t beg = time(NULL);
	struct rendo_context context;
	if (rendo_initialize(&context) != 0) {
		ERROR("context initalization failed.\n");
		return 1;
	}
	INFO("context initialization successful.\n");

	INFO("creating %d buffer objects...\n", BO_TEST_NUM);
	struct rendo_bo* object[BO_TEST_NUM];
	int i = 0;
	for (i = 0; i < BO_TEST_NUM; i++) {
		object[i] = rendo_bo_map(&context, 1024, 32, 0);
	}
	INFO("deleting %d buffer objects...\n", BO_TEST_NUM);
	for (i = 0; i < BO_TEST_NUM; i++) {
		rendo_bo_unmap(&context, object[i]);
	}

	rendo_destroy(&context);
	INFO("context destruction successful.\n");

	time_t end = time(NULL);
	printf("Unit test completed in %f seconds\n", difftime(end, beg));

	return 0;
}
