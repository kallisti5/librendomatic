/*
 * Copyright 2013-2014, Alexander von Gluck IV <kallisti5@unixzen.com>
 * Distributed under the terms of the MIT License.
 */


#include <stdio.h>
#include <inttypes.h>

#include "debug.h"
#include "rendomatic.h"


int
main()
{
	struct rendo_context context;
	if (rendo_initialize(&context) != 0) {
		ERROR("context initalization failed.\n");
		return 1;
	}
	INFO("context initialization successful.\n");

	rendo_destroy(&context);
	INFO("context destruction successful.\n");
	return 0;
}
