/*
 * Copyright 2013-2014, Alexander von Gluck IV <kallisti5@unixzen.com>
 * Distributed under the terms of the MIT License.
 */


// Generic portable spinlocks
void lock(volatile int* lock);
void unlock(volatile int* lock);
