/*
 * Copyright 2013-2014, Alexander von Gluck IV <kallisti5@unixzen.com>
 * Distributed under the terms of the MIT License.
 */


#include "rendomatic.h"

#include <stdlib.h>

#include "backend.h"
#include "debug.h"
#include "entry.h"
#include "util.h"


struct rendo_bo*
rendo_bo_map(struct rendo_context* context, size_t size, uint32_t alignment,
	uint32_t flags)
{
	CALLED();
	struct rendo_bo* bo = calloc(1, sizeof(struct rendo_bo));
	if (!bo) {
		ERROR("%s: Couldn't malloc new buffer object!\n", __func__);
		return NULL;
	}

	rendo_bo_lock(bo);
	bo->size = size;
	bo->alignment = alignment;
	bo->flags = flags;

	if (bo_map(context, bo) != 0) {
		ERROR("%s: Backend failed to map buffer object!\n", __func__);
		free(bo);
		return NULL;
	}

	rendo_bo_unlock(bo);
	return bo;
}


void
rendo_bo_unmap(struct rendo_context* context, struct rendo_bo* bo)
{
	CALLED();

	if (!bo) {
		TRACE("%s: BO is already free!\n", __func__);
		return;
	}

	rendo_bo_lock(bo);

	bo_unmap(context, bo);

	free(bo);
}


void
rendo_bo_lock(struct rendo_bo* bo)
{
	lock(&bo->lock);
}


void
rendo_bo_unlock(struct rendo_bo* bo)
{
	unlock(&bo->lock);
}
