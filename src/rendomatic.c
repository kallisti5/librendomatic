/*
 * Copyright 2013-2016, Alexander von Gluck IV <kallisti5@unixzen.com>
 * Distributed under the terms of the MIT License.
 */


#include "rendomatic.h"

#include <stdlib.h>

#include "backend.h"
#include "debug.h"
#include "entry.h"


static bool
context_check(struct rendo_context* context)
{
	if (!context || context->valid != true)
		return false;
	return true;
}


int
rendo_initialize(struct rendo_context* context)
{
	CALLED();

	TRACE("Using %s backend.\n", BACKEND_NAME);

	context->handle = open_device();
	int ret = connect_device(context);

	if (!ret)
		context->valid = true;

	return ret;
}


void
rendo_destroy(struct rendo_context* context)
{
	CALLED();

	if (!context || !context->valid) {
		ERROR("%s: Passed an invalid context!\n", __func__);
		return;
	}

	close_device(context);
}


void
rendo_lock(struct rendo_context* context, rendo_lock_flags flags)
{
	CALLED();

	if (!context || !context->valid) {
		ERROR("%s: Passed an invalid context!\n", __func__);
		return;
	}

	lock_device(context, flags);
}


void
rendo_unlock(struct rendo_context* context)
{
	CALLED();

	if (!context || !context->valid) {
		ERROR("%s: Passed an invalid context!\n", __func__);
		return;
	}

	unlock_device(context);
}


addr_t
rendo_base(struct rendo_context* context)
{
	CALLED();

	if (!context_check(context)) {
		ERROR("%s: Invalid context!\n", __func__);
		return 0;
	}

	return base_device(context);
}
