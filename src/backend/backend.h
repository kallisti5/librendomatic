/*
 * Copyright 2013-2014, Alexander von Gluck IV <kallisti5@unixzen.com>
 * Distributed under the terms of the MIT License.
 *
 * Standardized template for the backend functions
 */
#ifndef _BACKEND_H
#define _BACKEND_H


#include <stdbool.h>

#include "rendomatic.h"


int open_device();
void close_device(struct rendo_context* context);
bool connect_device(struct rendo_context* context);

void lock_device(struct rendo_context* context, rendo_lock_flags flags);
void unlock_device(struct rendo_context* context);

addr_t base_device(struct rendo_context* context);

int bo_map(struct rendo_context* context, struct rendo_bo* bo);
void bo_unmap(struct rendo_context* context, struct rendo_bo* bo);


/*
 Example DRM calls and how we could remake them.

func add_memory_mapping    // aka drmAddMap
func delete_memory_mapping // aka drmRmMap
func map_memory	// aka drmMap (uses memory allocated by drmAddMap)
func unmap_memory // aka drmUnmap

func add_buffers // aka drmAddBufs
func mark_buffers // aka drmMarkBufs
func free_buffers // aka drmFreeBufs
func buffer_info // aka drmGetBufInfo
func map_buffers // aka drmMapBufs (map buffers into user space)
func unmap_buffers // aka drmUnmapBufs

func reserve_dma // aka drmDMA
func lock_hardware // aka drmGetLock
func unlock_hardware // aka drmUnlock

drmGetReservedContextList??
drmFreeReservedContextList??

drmCreateContext
drmSwitchToContext
drmSetContextFlags
drmGetContextFlags
drmDestroyContext

drmCreateDrawable
drmDestroyDrawable
drmUpdateDrawableInfo

drmAgpAcquire
drmAgpRelease
drmAgpEnable
drmAgpAllocate
drmAgpFree
drmAgpBind
drmAgpUnbind
drmAgpVersionMajor
drmAgpVersionMinor
drmAgpGetMode
drmAgpBase
drmAgpSize
drmAgpMemoryUsed
drmAgpMemoryAvail
drmAgpVendorID
drmAgpDeviceID

drmScatterGatherAlloc
drmScatterGatherFree
drmWaitVBlank

drmCtlInstHandler
drmCtlUninstHandler
drmFinish

drmGetInterruptFromBusID
drmAddContextTag
drmDelContextTag
drmGetContextTag

drmAddContextPrivateMapping
drmGetContextPrivateMapping
drmGetMap
drmGetClient
drmGetStats

drmSetInterfaceVersion

// Device-specific commands
drmCommandNone
drmCommandRead
drmCommandWrite
drmCommandWriteRead

drmOpenOnce
drmCloseOnce
drmSetMaster
drmDropMaster
drmGetDeviceNameFromFd
drmPrimeHandleToFD
drmPrimeFDToHandle
*/

#endif /* _BACKEND_H */
