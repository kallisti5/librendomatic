/*
 * Copyright 2013-2016, Alexander von Gluck IV <kallisti5@unixzen.com>
 * Distributed under the terms of the MIT License.
 */
#ifndef _ENTRY_H
#define _ENTRY_H


#include "rendomatic.h"

#include <drm.h>
#include <xf86drm.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <va/va_dricommon.h>


#define BACKEND_NAME	"DRI"

struct backend_context {
	int			valid;
	drm_auth_t	auth;
	drm_version_t version;
	Display*	display;
	char*		driver;
	char*		device;
};


#endif /* _ENTRY_H */
