/*
 * Copyright 2013-2014, Alexander von Gluck IV <kallisti5@unixzen.com>
 * Distributed under the terms of the MIT License.
 *
 * Authors:
 *      Alexander von Gluck IV <kallisti5@unixzen.com>
 */


#include "backend.h"


int
bo_map(struct rendo_context* context, struct rendo_bo* bo)
{
	// TODO: DRI Buffer Object Mapping
	return 0;
}

void
bo_unmap(struct rendo_context* context, struct rendo_bo* bo)
{
	// TODO: DRI Buffer Object Unmapping
}
