/*
 * Copyright 2013-2016, Alexander von Gluck IV <kallisti5@unixzen.com>
 * Distributed under the terms of the MIT License.
 *
 * Authors:
 *		Alexander von Gluck IV <kallisti5@unixzen.com>
 */


#include "entry.h"

#include "debug.h"

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/ioctl.h>


static int
drm_ioctl(int handle, int command, void* data)
{
	int result = -1;
	do {
		result = ioctl(handle, command, data);
	} while (result == -1 && (errno == EINTR || errno == EAGAIN));
	return result;
}


int
open_device()
{
	CALLED();

	DIR *directory = opendir("/dev/dri");
	if (!directory)
		return -1;

	int device = -1;
	int count = 0;
	struct dirent* entry;
	char path[PATH_MAX];
	while ((entry = readdir(directory)) != NULL) {
		if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, ".."))
			continue;

		// If entry does *not* have card in name, continue
		if (strncmp(entry->d_name, "card", 4))
			continue;

		if (device >= 0) {
			close(device);
			device = -1;
		}

		sprintf(path, "/dev/dri/%s", entry->d_name);
		device = open(path, O_RDWR);
		if (device >= 0)
			count++;

		TRACE("%s: Found card /dev/dri/%s\n", __func__, entry->d_name);
	}

	if (count == 0) {
		ERROR("%s: no graphics devices found!\n", __func__);
		return -1;
	}

	return device;
}


bool
connect_device(struct rendo_context* context)
{
	CALLED();

	context->backend = (void*)calloc(1, sizeof(struct backend_context));
	struct backend_context* beContext = context->backend;

	if (!beContext) {
		ERROR("%s: allocation error in backend private context\n", __func__);
		return 1;
	}

	// BUG: This style of DRM auth died a while ago?
	// https://www.x.org/wiki/Events/XDC2013/XDC2013DavidHerrmannDRMSecurity/slides.pdf

#if 0
	if (drm_ioctl(context->handle, DRM_IOCTL_GET_MAGIC, &beContext->auth)) {
		ERROR("%s: unable to get DRI2 magic!\n", __func__);
		return 1;
	}

	TRACE("%s: obtained DRI2 magic: 0x%X\n", __func__, beContext->auth.magic);

	if (drm_ioctl(context->handle, DRM_IOCTL_AUTH_MAGIC, &beContext->auth)) {
		ERROR("%s: unable to authenticate to DRI2\n", __func__, errno);
		return 1;
	}
#endif

	TRACE("%s: authenticated to DRI2\n", __func__);

	beContext->version.name_len = 0;
	beContext->version.name = NULL;
	beContext->version.desc_len = 0;
	beContext->version.desc  = NULL;
	beContext->version.date_len = 0;
	beContext->version.date = NULL;

	if (drm_ioctl(context->handle, DRM_IOCTL_VERSION, &beContext->version)) {
		ERROR("%s: unable to probe DRI2 card information\n", __func__);
		return 1;
	}

	if (beContext->version.name_len)
		beContext->version.name = malloc(beContext->version.name_len + 1);
	if (beContext->version.date_len)
		beContext->version.date = malloc(beContext->version.date_len + 1);
	if (beContext->version.desc_len)
		beContext->version.desc = malloc(beContext->version.desc_len + 1);

	if (drm_ioctl(context->handle, DRM_IOCTL_VERSION, &beContext->version)) {
		ERROR("%s: unable to probe DRI2 card information\n", __func__);
		return 1;
	}

	beContext->version.name[beContext->version.name_len] = '\0';
	beContext->version.desc[beContext->version.desc_len] = '\0';
	beContext->version.date[beContext->version.date_len] = '\0';

	TRACE("%s: info: %s, %s, %s\n", __func__, beContext->version.name,
		beContext->version.desc, beContext->version.date);

	beContext->valid = 1;

	return 0;
}


void
lock_device(struct rendo_context* context, rendo_lock_flags flags)
{
	CALLED();
	if (!context || !context->backend)
		return;

	struct backend_context* beContext = context->backend;
}


void
unlock_device(struct rendo_context* context)
{
	CALLED();
	if (!context || !context->backend)
		return;

	struct backend_context* beContext = context->backend;

	return;
}


void
close_device(struct rendo_context* context)
{
	CALLED();

	if (!context || !context->backend)
		return;

	struct backend_context* beContext = context->backend;

	if (context->handle >= 0)
		close(context->handle);

	free(beContext);
}


addr_t
base_device(struct rendo_context* context)
{
    #warning DRI: TODO: Get card base address!
	return 0;
}
