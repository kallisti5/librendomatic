/*
 * Copyright 2013-2014, Alexander von Gluck IV <kallisti5@unixzen.com>
 * Distributed under the terms of the MIT License.
 *
 * Authors:
 *		Alexander von Gluck IV <kallisti5@unixzen.com>
 *		Artur Wyszynski <harakash@gmail.com>
 */


#include "entry.h"

#include "debug.h"

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <FindDirectory.h>


int
open_device()
{
	CALLED();

	DIR *directory = opendir("/dev/graphics");
	if (!directory)
		return -1;

	int device = -1;
	int count = 0;
	struct dirent* entry;
	char path[PATH_MAX];
	while ((entry = readdir(directory)) != NULL) {
		if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")
			|| !strcmp(entry->d_name, "vesa")) {
			continue;
		}

		if (device >= 0) {
			close(device);
			device = -1;
		}

		sprintf(path, "/dev/graphics/%s", entry->d_name);
		device = open(path, B_READ_WRITE);
		if (device >= 0)
			count++;
	}

	// Open VESA driver if we were not able to get a better one
	// for debugging purpose now, vesa accelerant does not have 3D acceleration
	if (count == 0) {
		device = open("/dev/graphics/vesa", B_READ_WRITE);
		if (device >= 0)
			count++;
	}

	if (count == 0) {
		ERROR("%s: no graphics devices found!\n", __func__);
		return B_ENTRY_NOT_FOUND;
	}

	closedir(directory);
	return device;
}


bool
connect_device(struct rendo_context* context)
{
	CALLED();
	char signature[1024];
	image_id imageID = -1;
	GetAccelerantHook accelerantHook;
	engine_token* engineToken;
	sync_token* syncToken;

	context->backend = (void*)calloc(1, sizeof(struct backend_context));
	struct backend_context* beContext = context->backend;

	if (beContext == NULL) {
		ERROR("%s: allocation error in backend private context\n", __func__);
		return 1;
	}

	if (ioctl(context->handle, B_GET_ACCELERANT_SIGNATURE, &signature,
		sizeof(signature)) != B_OK)
	return 1;

	TRACE("%s: found accelerant signature: %s\n", __func__, signature);

	struct stat accelerantStat;
	const static directory_which dirs[] = {
		B_USER_ADDONS_DIRECTORY,
		B_BEOS_ADDONS_DIRECTORY
	};

	int32 i;
	for (i = 0; i < 3; i++) {
		char path[PATH_MAX];
		if (find_directory(dirs[i], -1, false, path, PATH_MAX) != B_OK)
			continue;

		strcat(path, "/accelerants/");
		strcat(path, signature);
		if (stat(path, &accelerantStat) != 0)
			continue;

		imageID = load_add_on(path);
		if (imageID >= 0) {
			if (get_image_symbol(imageID, B_ACCELERANT_ENTRY_POINT,
					B_SYMBOL_TYPE_ANY, (void**)(&accelerantHook)) != B_OK ) {
				ERROR("%s: unable to get B_ACCELERANT_ENTRY_POINT\n", __func__);
				unload_add_on(imageID);
				imageID = -1;
				return 1;
			}

			beContext->InitAccelerant = (init_accelerant)accelerantHook(
				B_INIT_ACCELERANT, NULL);
			if (!beContext->InitAccelerant
				|| beContext->InitAccelerant(context->handle) != B_OK) {
				ERROR("%s: InitAccelerant failed\n", __func__);
				unload_add_on(imageID);
				imageID = -1;
				return 1;
			}

			break;
		}
	}

	if (imageID < B_OK)
		goto error;

	beContext->UninitAccelerant
		= (uninit_accelerant)accelerantHook(B_UNINIT_ACCELERANT, NULL);
	beContext->AcquireEngine
		= (acquire_engine)accelerantHook(B_ACQUIRE_ENGINE, NULL);
	beContext->ReleaseEngine
		= (release_engine)accelerantHook(B_RELEASE_ENGINE, NULL);

	beContext->GetDeviceInfo
		= (get_accelerant_device_info)accelerantHook(B_GET_ACCELERANT_DEVICE_INFO, NULL);
	beContext->GetFrameBufferInfo
		= (get_frame_buffer_config)accelerantHook(B_GET_FRAME_BUFFER_CONFIG, NULL);

	assert(beContext->UninitAccelerant);
	assert(beContext->AcquireEngine);
	assert(beContext->ReleaseEngine);
	assert(beContext->GetDeviceInfo);
	assert(beContext->GetFrameBufferInfo);

	beContext->GetDeviceInfo(&beContext->cardInfo);
	beContext->GetFrameBufferInfo(&beContext->framebufferInfo);

	// Dump some card info, we may want to validate the sanity once we start using this info.
	TRACE("%s: %s %s (%s), %d MB\n", __func__, beContext->cardInfo.name, beContext->cardInfo.chipset,
		beContext->cardInfo.serial_no, beContext->cardInfo.memory / 1000000);

	TRACE("%s: FrameBuffer virt: %p, FrameBuffer phys: %p, BytesPerRow: %d\n", __func__,
		beContext->framebufferInfo.frame_buffer, beContext->framebufferInfo.frame_buffer_dma,
		beContext->framebufferInfo.bytes_per_row);

	if (beContext->AcquireEngine(B_3D_ACCELERATION, 0xff, syncToken,
		&engineToken) < B_OK) {
		ERROR("%s: accelerant doesn't support 3D acceleration\n", __func__);
		goto error;
	}

	TRACE("%s: accelerant supports 3D acceleration\n", __func__);

	beContext->ReleaseEngine(engineToken, syncToken);

	beContext->valid = 1;
	beContext->accelerantImageID = imageID;
	beContext->engineToken = engineToken;
	beContext->syncToken = syncToken;

	#if 0
	info->GetVersion = (drm_get_version)accelerantHook(B_DRM_GET_VERSION, NULL);
	info->DebugPrint = (drm_debug_printf)accelerantHook(B_DRM_DEBUG_PRINT, NULL);
	info->Ioctl = (drm_ioctl)accelerantHook(B_DRM_IOCTL, NULL);
	#endif
	return 0;

error:
	if (beContext->UninitAccelerant)
		beContext->UninitAccelerant();
	unload_add_on(imageID);
	if (beContext)
		free(beContext);
	return 1;
}


void
lock_device(struct rendo_context* context, rendo_lock_flags flags)
{
	CALLED();
	if (!context || !context->backend)
		return;

	//struct backend_context* beContext = context->backend;
	#warning TODO: Implement locking on Haiku via accelerant.
}


void
unlock_device(struct rendo_context* context)
{
	CALLED();
	if (!context || !context->backend)
		return;

	//struct backend_context* beContext = context->backend;
	#warning TODO: Implement unlocking on Haiku via accelerant.
}


void
close_device(struct rendo_context* context)
{
	CALLED();

	if (!context || !context->backend)
		return;

	struct backend_context* beContext = context->backend;

	beContext->UninitAccelerant();
	unload_add_on(beContext->accelerantImageID);

	if (context->handle >= 0)
		close(context->handle);

	free(beContext);
}


addr_t
base_device(struct rendo_context* context)
{
	CALLED();

	#warning HAIKU: TODO: Get card base address!
	return 0;
}
