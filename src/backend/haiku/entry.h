/*
 * Copyright 2013-2014, Alexander von Gluck IV <kallisti5@unixzen.com>
 * Distributed under the terms of the MIT License.
 */
#ifndef _BE_BASICS_H
#define _BE_BASICS_H


#include <Accelerant.h>
#include <graphic_driver.h>
#include <image.h>
#include <stdarg.h>
#include <sys/types.h>
#include <stdint.h>

#include "rendomatic.h"
#include "backend.h"


#define BACKEND_NAME	"Haiku"


struct backend_context {
	int valid;
	image_id accelerantImageID;
	accelerant_device_info cardInfo;
	frame_buffer_config framebufferInfo;
	engine_token* engineToken;
	sync_token* syncToken;

	// Functions passed by accelerant
	GetAccelerantHook AccelerantHook;
	init_accelerant InitAccelerant;
	uninit_accelerant UninitAccelerant;
	acquire_engine AcquireEngine;
	release_engine ReleaseEngine;
	get_accelerant_device_info GetDeviceInfo;
	get_frame_buffer_config GetFrameBufferInfo;
};


#endif /* _BE_BASICS_H */
