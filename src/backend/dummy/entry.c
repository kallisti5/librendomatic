/*
 * Copyright 2013-2014, Alexander von Gluck IV <kallisti5@unixzen.com>
 * Distributed under the terms of the MIT License.
 *
 * Authors:
 *		Alexander von Gluck IV <kallisti5@unixzen.com>
 */


#include "entry.h"

#include "debug.h"

#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>


int
open_device()
{
	CALLED();
	return -1;
}


bool
connect_device(struct rendo_context* context)
{
	CALLED();
	return 0;
}


void
close_device(struct rendo_context* context)
{
	CALLED();

	if (!context || !context->backend)
		return;
}


addr_t
base_device(struct rendo_context* context)
{
	CALLED();
    return 0;
}
