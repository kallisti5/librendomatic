/*
 * Copyright 2013-2014, Alexander von Gluck IV <kallisti5@unixzen.com>
 * Distributed under the terms of the MIT License.
 */
#ifndef _DEBUG_H
#define _DEBUG_H


#include <stdio.h>

#define DEBUG
#ifdef DEBUG
#   define TRACE(x...) printf("librendomatic-trace: " x)
#else
#   define TRACE(x...) ;
#endif

#define INFO(x...) printf("librendomatic-info: " x)
#define ERROR(x...) printf("librendomatic-error: " x)
#define CALLED() TRACE("%s()\n", __PRETTY_FUNCTION__)


#endif /* _DEBUG_H */
