/*
 * Copyright 2013-2014, Alexander von Gluck IV <kallisti5@unixzen.com>
 * Distributed under the terms of the MIT License.
 */


// Generic portable spinlocks
void
lock(volatile int* lock)
{
	while (__sync_lock_test_and_set(lock, 1))
		while (*lock)
			;
}


void
unlock(volatile int* lock)
{
	__sync_lock_release(lock);
}
